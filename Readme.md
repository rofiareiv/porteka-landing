## Porteka Template Landing Pange

- Template Name: Tempo
- Template URL: https://bootstrapmade.com/tempo-free-onepage-bootstrap-theme/
- Author: BootstrapMade.com
- License: https://bootstrapmade.com/license/

## Command line instructions
You can also upload existing files from your computer using the instructions below.


# Git global setup
```
git config --global user.name "Rofiuddin"
git config --global user.email "kambing.rofi@gmail.com"
```

## Clone repository
```
git clone https://gitlab.com/rofiareiv/porteka-landing.git
cd porteka-landing
```
## Make another branch
```
git branch name_branch
git checkout name_branch
```
## Push an changed branch
```
git add .
git commit -m "notes"
git push -u origin master
```
